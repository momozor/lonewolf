;;; Handles database connections
(defpackage lonewolf.database
  (:use :cl)
  (:import-from :datafly
                :*connection*)
  (:import-from :cl-dbi
                :connect)
  (:export :db
           :with-connection))
(in-package :lonewolf.database)

(defparameter *application-root-path*
  (asdf:system-source-directory :lonewolf))

(defun db ()
  (connect :sqlite3
           :database-name
           (merge-pathnames 
            *application-root-path*
            "lonewolf-config.db")))

(defmacro with-connection (conn &body body)
  `(let ((*connection* ,conn))
     ,@body))
