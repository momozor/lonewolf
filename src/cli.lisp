;;; Handles command-line interface
(defpackage lonewolf.cli
  (:use :cl)
  (:import-from :lonewolf.crud
                :create-bug
                :get-bug
                :get-all-bugs
                :update-bug
                :delete-bug)
  (:import-from :cl-ansi-text
                :red
                :green
                :blue
                :yellow
                :cyan))
(in-package :lonewolf.cli)

(defun prompt-read (prompt)
  (format *query-io* "~a: " prompt)
  (force-output *query-io*)
  (read-line *query-io*))

(defun create-bug-prompt ()
  (format t (green "~%Create a new bug~%" :effect :bright))
  (cond ((create-bug
          (prompt-read "Title")
          (prompt-read "Description")
          (prompt-read "Label")
          (prompt-read "Priority"))
         (format t (green "~%Bug Creation successful~%"
                          :effect :bright)))

        (t
         (format t (red "~%Bug Creation failed~%"
                        :effect :bright)))))

(defun print-all-bugs ()
  (dolist (i (get-all-bugs))

    (format t "ID: ~a  Title: ~a  Description: ~a Label: ~a Priority: ~a~%"
            (second i)
            (fourth i)
            (sixth i)
            (eighth i)
            (tenth i))))

(defun update-bug-prompt ()
  (format t (cyan "~%Update an existing bug~%" :effect :bright))
  (cond ((update-bug
          (prompt-read "ID")
          :title (prompt-read "Title")
          :description (prompt-read "Description")
          :label (prompt-read "Label")
          :priority (prompt-read "Priority"))
         (format t (green "~%Bug Update was successful~%"
                          :effect :bright)))

        (t
         (format t (red "~%Bug Update failed~%"
                        :effect :bright)))))

(defun delete-bug-prompt ()
  (format t (cyan "~%Delete an existing bug~%" :effect :bright))
  (cond ((delete-bug
          (prompt-read "ID"))
         (format t (green "~%Bug Deletion successful~%"
                          :effect :bright)))

        (t
         (format t (red "~%Bug Deletion failed~%"
                        :effect :bright)))))

(defun get-toplevel-choice (choice)
  (cond ((or (string= choice "create")
             (string= choice "c"))
         (create-bug-prompt))

        ((or (string= choice "list-all")
             (string= choice "la"))
         (print-all-bugs))

        ((or (string= choice "update")
             (string= choice "u"))
         (update-bug-prompt))

        ((or (string= choice "delete")
             (string= choice "d"))
         (delete-bug-prompt))

        ((string= choice "")
         (format t (red "~%Please type a choice above~%"
                        :effect :bright))
         (main-menu-prompt))

        (t
         (format t (red "~%Please type a choice above~%"
                        :effect :bright))
         (main-menu-prompt))))

(defun main-menu-prompt ()
  (format t "Menu: [create/c] [list-all/la] [update/u] [delete/d]~%")
  (format t "Type your choice to continue..~%~%")
  (get-toplevel-choice (prompt-read "Choice")))

(defun repl ()
  (format t "~%Welcome to ~a!~%~%"
          (cyan "Lonewolf" :effect :bright))
  (main-menu-prompt))
