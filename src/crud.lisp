;;; Handles CREATE, READ, UPDATE AND DELETE stuff
(defpackage lonewolf.crud
  (:use :cl :sxql)
  (:import-from :lonewolf.database
                :db
                :with-connection)
  (:import-from :datafly
                :execute
                :retrieve-one
                :retrieve-all)
  (:export :create-bug
           :get-bug
           :get-all-bugs
           :update-bug
           :delete-bug))
(in-package :lonewolf.crud)

(defmacro with-db (&body body)
  "simplify datafly query execution calls"
  `(with-connection (db)
     (execute
      ,@body)))

(defun data-valid-p (title description)
  "make sure length of title and description
   of the input passes certain checks"
  (and
   (and (> (length title) 5)
        (< (length title) 100))
   (and (> (length description) 10)
        (< (length description) 500))))

(defun get-current-time () 
  (multiple-value-bind
        (second minute hour day month year)
      (get-decoded-time)
    (format nil
            "~a:~a:~a ~a-~a-~a"
            hour minute second
            day month year)))

(defun create-table-if-not-exists ()
  (with-db 
    (create-table (:lonewolf :if-not-exists t)
        ((id :type 'integer :primary-key t)
         (title :type 'text :not-null t)
         (description :type 'text :not-null t)
         (label :type 'text :not-null t)
         (priority :type 'text :not-null t)
         (date-published :type 'text :not-null t)))))

(defun add-bug (title description
                &optional label priority)
  "used by CREATE-BUG for direct db access"
  (create-table-if-not-exists)
  (with-db
    (insert-into :lonewolf
      (set= :title title
            :description description
            :label label
            :priority priority
            :date-published (get-current-time)))))

(defun extra-parameters-empty-p (&optional label priority)
  (cond ((and (string= label "")
              (string= priority ""))
         (values "New" "Low"))

        ((string= label "")
         (values "New" priority))

        ((string= priority "")
         (values label "Low"))

        (t
         (values label priority))))

(defun create-bug (title description
                   &optional (label "New") (priority "Low"))
  "create and save new bug list into the database"
  (cond ((data-valid-p title description)
         
         ;; guarantee that even empty strings will reset them
         ;; to the defaults
         (add-bug title description
                  (nth-value 0
                             (extra-parameters-empty-p label priority))
                  (nth-value 1
                             (extra-parameters-empty-p label priority)))
         t)))

(defun get-bug (id)
  "get a single bug list from the database, using
   the bug id as an identifier"
  (with-connection (db)
    (retrieve-one
     (select :* (from :lonewolf)
             (where (:= :id id))))))

(defun get-all-bugs ()
  "get all bugs from the database"
  (with-connection (db)
    (retrieve-all
     (select :* (from :lonewolf)))))

(defun get-bug-data (id key)
  "get all the properties of a bug list"
  (let ((bug-data (get-bug id)))
    (getf bug-data key)))

;; TODO add an exception if update-bug goes
;; wrong
;; i.e updating a non-existent bug
(defun update-bug (id &key
                        (title (get-bug-data id :title))
                        (description (get-bug-data id :description))
                        (label (get-bug-data id :label))
                        (priority (get-bug-data id :priority)))
  "update an existing bug list and use default parameters
   if none supplied because it will set the parameters to null/nill instead
   which will trigger sql exception on the db side"
  (data-valid-p title description)
  (with-db
    (update :lonewolf
      (set= :title title
            :description description
            :label label
            :priority priority)
      (where (:= :id id))))
  t)

;; TODO add an exception if bug deletion
;; goes wrong
;; i.e deleting a non-existing bug
(defun delete-bug (id) 
  (with-db
    (delete-from :lonewolf
      (where (:= :id id))))
  t)
