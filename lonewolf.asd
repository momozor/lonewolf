(defsystem "lonewolf"
  :version "0.1.0"
  :author "Momozor"
  :license "AGPL-3.0"
  :depends-on ("datafly"
               "sxql"
               "cl-ansi-text")
  :components ((:module "src"
                        :components
                        ((:file "main" :depends-on ("crud" "cli" "crud" "database"))
                         (:file "cli" :depends-on ("crud" "database"))
                         (:file "crud" :depends-on ("database"))
                         (:file "database"))))
  :description "A simple command-line bug tracker"
  :in-order-to ((test-op (test-op "lonewolf/tests")))
  :build-operation "program-op" ;; leave as is
  :build-pathname "lonewolf"
  :entry-point "lonewolf.cli::repl")

(defsystem "lonewolf/tests"
  :author "Momozor"
  :license "AGPL-3.0"
  :depends-on ("lonewolf"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for lonewolf"
  :perform (test-op (op c) (symbol-call :rove :run c)))
