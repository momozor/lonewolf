(defpackage lonewolf/tests/main
  (:use :cl
        :rove))
(in-package :lonewolf/tests/main)

;;; NOTE: To run this test file, execute `(asdf:test-system :lonewolf)' in your Lisp.
;;; NOTE: All destructive and non-destructive related queries to the database are skipped.

(defparameter *title* "omega interprata equinor")
(defparameter *description* "omega is interpreted in a very non-effective way")

(defun time-now ()
  (multiple-value-bind
        (second minute hour day month year)
      (get-decoded-time)
    (format nil
            "~a:~a:~a ~a-~a-~a"
            hour minute second
            day month year)))

(defun exp-data-1 ()
  (lonewolf.crud::extra-parameters-empty-p "" "High"))

(defun exp-data-2 ()
  (lonewolf.crud::extra-parameters-empty-p "Bug" ""))

(defun exp-data-3 ()
  (lonewolf.crud::extra-parameters-empty-p "" ""))

(defun exp-data-4 ()
  (lonewolf.crud::extra-parameters-empty-p "Bug" "Medium"))

(deftest crud
  (testing "data-valid-p should return t"
    (ok
     (lonewolf.crud::data-valid-p *title* *description*)))
  
  (testing "current time is equal to cpu time"
    (ok
     (string= (lonewolf.crud::get-current-time)
              (time-now))))

  (testing "extra-parameters-empty-p to return New and High"
    (ok
     (and (string= "New" (nth-value 0 (exp-data-1)))
          (string= "High" (nth-value 1 (exp-data-1))))))

  (testing "extra-parameters-empty-p to return Bug and Low"
    (ok
     (and (string= "Bug" (nth-value 0 (exp-data-2)))
          (string= "Low" (nth-value 1 (exp-data-2))))))

  (testing "extra-parameters-empty-p to return New and Low"
    (ok
     (and (string= "New" (nth-value 0 (exp-data-3)))
          (string= "Low" (nth-value 1 (exp-data-3))))))

  (testing "extra-parameters-empty-p to return Bug and Medium"
    (ok
     (and (string= "Bug" (nth-value 0 (exp-data-4)))
          (string= "Medium" (nth-value 1 (exp-data-4)))))))
